package main

import (
	"fmt"

	"rsc.io/quote"
)

func main() {
	notCoveredFunc("Hello")
}

func notCoveredFunc(s string) {
	fmt.Println(s)
	fmt.Println(Hello())
}

func Hello() string {
	return quote.Hello()
}
