.PHONY: dependencies
dependencies: ## Download dependencies
	@echo "Download dependencies..."
	@go mod download
	@echo "Done"

.PHONY: build
build: ## build for linux/amd64 platform
	@go version
	go build

.PHONY: test
test: ## Run tests for all packages (test cache is disabled)
	go test ./... \
		-count=1 \
 		-coverprofile cover-all.out

.PHONY: cover
cover: test ## Run coverage report
	gocover-cobertura < cover-all.out > cover-cobertura.xml

.PHONY: code-quality-print
code-quality-print: ## Run golang-cilint with printing to stdout
	golangci-lint run ./...

.PHONY: clean
clean: ## clean
	rm -rf ./vendor/
	rm -f cover-all.out  cover-cobertura.xml

COVERAGE_THRESHOLD=0

.PHONY: coverage-check
coverage-check: ## Check weather coverage is less than 80%
	echo "coverage is 14%"
